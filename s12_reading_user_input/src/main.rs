use std::io;

fn main() {
    let mut input = String::new();
    println!("Hey mate! Say something:");
    match io::stdin().read_line(&mut input){
        Ok(_) => {
            // println!("Success! You said: {}", input); //untuk output huruf kucil
            println!("Success! You said: {}", input.to_uppercase()); //untuk output huruf Besar
        },
        Err(e) => println!("Ooops! Something went wrong: {}", e)
    }
}
