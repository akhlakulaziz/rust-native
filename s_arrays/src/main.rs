fn main() {
    let numbers = [1, 2, 3, 4, 5];
    //cara pertama menampilkan array
    println!("================cara pertama menampilkan array========================");
    for n in numbers.iter() {
        println!("{}", n);
    }
        println!("");
    
    //cara kedua menampilkan array
    println!("================cara kedua menampilkan array========================");
    for i in 0..numbers.len(){
        println!("{}", numbers[i]);
    }
    println!("");

    //cara menampilkan array model ketiga
    println!("================cara menampilkan array model ketiga (menentukan jumlah array)========================");
    let numbers = [2; 400];
    for i in 0..numbers.len(){
        println!("{}", numbers[i]);
    }

}
