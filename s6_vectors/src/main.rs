fn main() {
    let mut my_vector = vec![1, 2, 3, 4];
    my_vector.push(49); //menambah 49 di array
    my_vector.remove(1); //remove '2' (array dimulai dari 0)

    for number in my_vector.iter() {
        println!("{}", number);
    }
}