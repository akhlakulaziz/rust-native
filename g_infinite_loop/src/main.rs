fn main() {
    let mut n = 0; //mut digunkan agar nilai variable n dapat dirubah

    loop {
        n += 1;
        if n == 7{
            continue;
        }
        if n > 10{
            break;
        }
        println!("nilai ke: {}", n);
    }
}
