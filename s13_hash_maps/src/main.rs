use std::collections::HashMap;

fn main() {
    let mut marks = HashMap::new();

    // add value
    marks.insert("Rust Programing", 96);
    marks.insert("Web Development", 94);
    marks.insert("UI Design", 75);
    marks.insert("Profesional Computing Studies", 45);

    //Find length of HashMap
    println!("How Many subject have you studies? {}", marks.len());

    //Get a single value
    match marks.get("Web Development"){
        Some(mark) => println!("You got {} for Web Dev!", mark),
        None => println!("You didn't study Web Development")
    }

    //remove a value
    marks.remove("UX Design");

    // Loop throught HashMap
    for (subject, mark) in &marks{
        println!("For {} you got {}%!", subject, mark);
    }

    //Check for value
    println!("Did you study C++? {}", marks.contains_key("C++ Programming"));
}
