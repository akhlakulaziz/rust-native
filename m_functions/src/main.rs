//contoh 1====================
// fn main() {
//     print_numbers_to(10);

//     if is_even(30) {
//         println!("it is event!");
//     }
// }

// fn print_numbers_to(num: u32){
//     for n in 1..num {
//         println!("{}", n);
//     }
// }

// fn is_even(num: u32) -> bool{
//     return num % 2 == 0;
// }

// contoh2==============
fn main() {
    print_numbers_to(20);

}

fn print_numbers_to(num: u32){
    for n in 1..num {
        
        if is_even(n) {
            println!("{} is event!", n);
        }else{
            println!("{} is odd!", n);
        }
    }
}

fn is_even(num: u32) -> bool{
    return num % 2 == 0;
}