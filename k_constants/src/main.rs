const MAXIMUM_NUMBER: u8 = 20;

fn main() {
    for n in 1..MAXIMUM_NUMBER {
        println!("{}", n);
    }
    // const MAXIMUM_NUMBER: u8 = 30;
}
