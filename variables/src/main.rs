fn main() {
    let mut x = 5; //mut diguakan agar nilai varibale x dapat diubah
    println!("The value of x is: {}", x);
    x = 6;
    println!("The value of x is: {}", x);
    println!("");

    //=======================contoh Variabel #2==========================================
    let x = 5;
    let x = x + 1;
    let x = x * 2;
    
    println!("==============contoh Variabel #2===================");
    println!("The value of x is: {}", x);
    println!("");


//==========================Operasi Aritmatika==================================
    // addition
    let sum = 5 + 10;
    println!("==============Operasi Aritmatika===================");
    println!("Penambahan: {}", sum);

    // subtraction
    let difference = 95.5 - 4.3;
    println!("Pengurangan: {}", difference);

    // multiplication
    let product = 4 * 30;
    println!("Perkalian: {}", product);

    // division
    let quotient = 56.7 / 32.2;
    println!("Pembagian: {}", quotient);

    // remainder
    let remainder = 43 % 5;
    println!("Sisa Pembagian: {}", remainder);

}
