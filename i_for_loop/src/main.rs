fn main() {
    // ===================for number====================
    // let numbers = 30..51;
    // for i in numbers {
    //     println!("The number is {}", i);
    // }

    // for array
    let animals = vec!["Rabbit", "Dog", "Cat"];

    for (index, a) in animals.iter().enumerate(){
        println!("The index is {} and the animal name is {}", index, a);
    }
}
