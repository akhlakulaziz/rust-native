fn main() {
    //cara menampilkan jumlah karakter string
    let my_string = String::from("how's it going? my name is Dom.");


    println!("==========cara menampilkan jumlah karakter string=============");
    //length
    println!("Length: {}", my_string.len());
    // is Empty?
    println!("String is empty? {}", my_string.is_empty());
    println!("");

    //cara memberi enter pada setiap kata yang ditulis
    println!("");
    println!("==========cara memberi enter pada setiap kata yang ditulis=============");
    for token in my_string.split_whitespace() {
        println!("{}", token)
    }

    println!("Does the string contain 'Dom'? {}", my_string.contains("Dom"));

    // my_string.push_str("welcome to your tutorial on Strings!");
    // println!("{}", my_string);
}
