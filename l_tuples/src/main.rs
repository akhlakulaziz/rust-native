fn main() {
    //number array
    // let tup1 = (20, 25, 30, 35);
    // println!("{}", tup1.2);

    // let tup1 = (20, "Rust", 3.4, false);
    // println!("{}", tup1.3);

    // let tup1 = (20, "Rust", 3.4, false, (1, 4, 7));
    // println!("{}", (tup1.4).2);

    let tup1 = (45, 6.7, "Computer");
    let (a, b, c) = tup1;
    println!("a is {}", a);
    println!("b is {}", b);
    println!("c is {}", c);
}
