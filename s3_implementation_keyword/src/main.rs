struct Rectangel{
    width: u32,
    height: u32
}

impl Rectangel{
    fn print_description(&self){
        println!("Rectangel: {} x {}", self.width, self.height);
    }

    fn is_square(&self) -> bool{
        self.width == self.height
    }
}

fn main() {
    let my_rect = Rectangel {width: 10, height: 5};
    my_rect.print_description();
    println!("Rectangle is a square: {}", my_rect.is_square())
}
