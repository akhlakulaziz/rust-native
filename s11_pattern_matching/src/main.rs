fn main() {
    let number = 15;

    match number {
        1 => println!("It is one!"),
        0 | 11 => println!("It is either 10 or 11"),
        _ => println!("It doesn't match!")
    }
    //berlaku dengan varchar
}
